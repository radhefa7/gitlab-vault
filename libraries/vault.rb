module GitLab
  module Vault
    # get(node, 'omnibus-gitlab')
    def self.get(node, *path)
      node_name = node["fqdn"] || node.name
      node_environment = node.chef_environment

#      Chef::Log.info("Looking up secret path #{path} for node #{node_name} by #{cookbook_name}[#{recipe_name}]")
      Chef::Log.info("Looking up secret path #{path} for node #{node_name}")

      node_attributes = get_node_attributes(node, path)

      node_secrets = get_node_secrets_from_attributes(node_attributes, path, node_environment)

      Chef::Log.info("Mergin secrets (#{node_secrets.keys}) into node attributes (#{node_attributes.keys})")

      Chef::Mixin::DeepMerge.deep_merge(
        node_secrets,
        node_attributes)
    end


    def self.get_node_secrets_from_node(node, *path)
      node_environment = node.chef_environment
      node_attributes = get_node_attributes(node, path)
      get_node_secrets_from_attributes(node_attributes, path, node_environment)
    end


    def self.get_node_attributes(node, path)
      node_name = node["fqdn"] || node.name
      Chef::Log.info("Loading node attributes for #{node_name} in path #{path}")
      unnest_attributes_by_path(node, path)
    end


    def self.get_node_secrets_from_attributes(node_attributes, path, node_environment)
      return {} unless node_attributes['chef_vault']

      chef_vault = node_attributes['chef_vault']
      chef_vault_item = node_attributes['chef_vault_item'] || node_environment

      Chef::Log.info("Decrypting #{chef_vault_item} contained in vault #{chef_vault}")

      secrets = ::ChefVault::Item.load(chef_vault, chef_vault_item).to_hash

      unnest_attributes_by_path(secrets, path)
    end

    # What we are doing here is pulling up whatever is inside the whole path to the top level.
    #
    # The reason why we are doing this is still a mystery, but the whole cookbooks are behaving this way.
    # So we can't change it (yet)
    #
    # fetch_path(hash, ['foo', 'bar', 'baz'])
    # => hash['foo']['bar']['baz']
    def self.fetch_path(node_attributes, path)
      unnest_attributes_by_path(node_attributes, path)
    end

    private

    def self.unnest_attributes_by_path(node_attributes, path) 
      path.each do |level|
        begin
          node_attributes = node_attributes[level]
        rescue => ex
          Chef::Log.error "AttributesWithSecrets: Invalid node_attributes path #{path.inspect}"
          raise ex
        end
      end
      (node_attributes || {}).to_hash
    end

  end
end
